var viavarejo = {

  mySwiper: null,

  init: function() {
    this.startSwiper();
    this.requestSidebar();
  },

  startSwiper: function() {
    this.mySwiper = new Swiper('.swiper-container', {
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      slidesPerView: 3,
      spaceBetween: 10
    });
  },

  requestSidebar: function() {
    $.ajax({
      type: 'GET',
      url: '/side_bar.json',
      success: function(resp) {
        viavarejo.buildSidebar(resp);
      },
      error: function() {
        console.log('problem on request');
      }
    });
  },

  buildSidebar: function(data) {
    var ordenada = this.sortResults(data, 'category');
    console.log(ordenada);
  },

  sortResults: function(data, key) {
    return data.sort(function(a, b) {
      var x = a[key];
      var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

};

var $ = jQuery.noConflict();

(function() {
  viavarejo.init();
})();