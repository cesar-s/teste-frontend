'use strict';

var argv = require('minimist')(process.argv.slice(2));
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var cssNano = require('gulp-cssnano');
var gulp = require('gulp');
var sassRuby = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
// var versionAppend = require('gulp-version-append');
// var svgmin = require('gulp-svgmin');

// [ prod(1) / dev(0) ]
var $env = (argv.prod)? 1 : 0;

/*----------------------------------------------
	Default
-----------------------------------------------*/
gulp.task('default',function(){
	gulp.start('sass');
	gulp.start('vendor-css');
	gulp.start('js');
	gulp.start('vendor-js');
	// gulp.start('svgmin');
	// gulp.start('change-version');
});


/*----------------------------------------------
	Watch CSS & JS
-----------------------------------------------*/
gulp.task('watch',function(){

	gulp.start('default');

	gulp.watch('app/styles/src/*.scss',function(e){
		console.log('['+e.type + '] ' + e.path);
		gulp.start('sass');
		// gulp.start('change-version');
	});

	gulp.watch('app/scripts/src/*.js',function(e){
		console.log('['+e.type + '] ' + e.path);
		gulp.start('js');
		// gulp.start('change-version');
	});

});


/*----------------------------------------------
	Vendor CSS
-----------------------------------------------*/
gulp.task('vendor-css',function(){
	gulp.src([
		'app/styles/vendor/normalize.css',
		'app/styles/vendor/swiper.css'
	])
		.pipe(concat('vendor.min.css'))
		.pipe(cssNano({
			discardComments: { removeAll: true },
			zindex: false
		})).on('error', errorHandler)
		.pipe(gulp.dest('app/styles/'));
});


/*----------------------------------------------
	SASS
-----------------------------------------------*/
gulp.task('sass',function(){

	if($env === 1){

		sassRuby('app/styles/src/style.scss',{
			sourcemap: false
		})
		.pipe(autoprefixer({
			browsers : ['last 5 versions','ie 9'],
			cascade: true
		}))
		.on('error', errorHandler)
		.pipe(cssNano({
			discardComments: { removeAll: true },
			zindex: false
		}))
		.on('error', errorHandler)
		.pipe(rename('app.css'))
		.pipe(gulp.dest('app/styles'));

	} else {

		sassRuby('app/styles/src/style.scss',{
			sourcemap: true
		})
		.pipe(autoprefixer({
			browsers : ['last 5 versions','ie 9'],
			cascade: true
		}))
		.on('error', errorHandler)
		.pipe(cssNano({
			discardComments: { removeAll: true },
			zindex: false
		}))
		.on('error', errorHandler)
		.pipe(rename('app.css'))
		.pipe(sourcemaps.write('maps'))
		.pipe(gulp.dest('app/styles'));

	}

});


/*----------------------------------------------
	Vendor JS
-----------------------------------------------*/
gulp.task('vendor-js', function(){

	if($env === 1){

		gulp.src('app/scripts/vendor/*.js')
			.pipe(uglify())
			.pipe(stripDebug())
			.on('error', errorHandler)
			.pipe(rename(function(path){
				path.basename += '.min';
			}))
			.pipe(gulp.dest('app/scripts'));

	} else {

		gulp.src('app/scripts/vendor/*.js')
			.pipe(rename(function(path){
				path.basename += '.min';
			}))
			.pipe(gulp.dest('app/scripts'));

	}

});


/*----------------------------------------------
	Javascript
-----------------------------------------------*/
gulp.task('js', function(){

	if($env === 1){

		gulp.src('app/scripts/src/*.js')
			.pipe(uglify())
			.pipe(stripDebug())
			.on('error', errorHandler)
			.pipe(rename(function(path){
				path.basename += '.min';
			}))
			.pipe(gulp.dest('app/scripts'));

	} else {

		gulp.src('app/scripts/src/*.js')
			.pipe(rename(function(path){
				path.basename += '.min';
			}))
			.pipe(gulp.dest('app/scripts'));

	}

});


/*----------------------------------------------
	IMAGES - SVG
-----------------------------------------------*/

// gulp.task('svgmin', function() {

// 	gulp.src('images/src/*.svg')
// 		.pipe(svgmin({
// 			plugins: [{
// 				cleanupNumericValues: {
// 					floatPrecision: 2
// 				}
// 			}]
// 		}))
// 		.pipe(gulp.dest('images/dist'));

// });

/*----------------------------------------------
	Version CSS & JS
-----------------------------------------------*/
// gulp.task('change-version', function() {

// 	var attrib;
// 	var mattrib;

// 	attrib = new Date().getTime().toString();
// 	mattrib = attrib.slice(0,1);

// 	gulp.src(['version.json'])
// 		.pipe(replace('vvv', attrib))
// 		.pipe(replace('mvv', mattrib))
// 		.pipe(gulp.dest('./'));

// });

// Handle the error
function errorHandler (error) {
	console.log(error.toString());
	this.emit('end');
}
